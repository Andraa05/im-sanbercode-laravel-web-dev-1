<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\castController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/master', function(){
    return view('page.master');
});
  
Route::get('/table', function(){
    return view('table');
});
Route::get('/datatable', function(){
    return view('datatable');
});
  
// CRUD
Route::get('/cast',[castController::class,'index']);
Route::get('/cast/create',[castController::class,'create']);
Route::post('/cast',[castController::class,'store']);
Route::get('/cast/{id}',[castController::class,'show']);
Route::get('/cast/{id}/edit', [castController::class,'edit']);
Route::put('/cast/{id}',[castController::class,'update']);
Route::delete('/cast/{id}',[castController::class,'destroy']);

