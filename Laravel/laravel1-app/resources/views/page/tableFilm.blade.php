@extends('page.master')
@section('title')
  Data pemain
@endsection

@section('sub title')
<a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah Data Cast</a>
@endsection

@section('content')
<table class="table">
  <thead?>
    <tr>
      <th scope="col">id</th>
      <th scope="col">name</th>
      <th scope="col">umur</th>
      <th scope="col">Bio</th>
    </tr>
</thead?>
<tbody>
  @forelse ($cast as $key => $item)
        <tr>
          <td>{{$key + 1}}</td>
          <td>{{$item -> name}}</td>
          <td>{{$item -> umur}}</td>
          <td>
            <form action="/cast/{{$item->id}}" method="POST">
              @csrf
              @method('delete')
              <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm" >detail</a>
              <a href="/cast/{{$item->id}}/edit" class="btn btn-secondary btn-sm" >Edit data</a>
              <input type="submit" value="delete" class="btn btn-danger btn-sm">
            </form>
          </td>
        </tr>
  @empty
       <tr>
        <td>Data cast</td>
       </tr>
       @endforelse
</tbody>
</table>
@endsection('content')