@extends('page.master')
@section('title')
  Tambah Data
@endsection

@section('sub title')
   Data cast
@endsection

@section('content')
<form action="/cast" method="post">
  @csrf
  <div class="form-group">
    <label>cast name</label>
    <input type="text" name="name" class="form-control" >
</div>
@error('name')
<div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label >umur</label>
    <input type="text" name="umur" class="form-control" >
</div>
@error('umur')
<div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
  </div>
  @error('Bio')
<div class="alert alert-danger">{{ $messeage }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection('content')