@extends('page.master')
@section('title')
  Halaman Detail Cast
@endsection

@section('sub title')
   Detail cast
@endsection

@section('content')

<h1>{{ $cast -> name }}</h1>
<h1>{{ $cast -> umur }}</h1>
<p>{{ $cast -> bio }}</p>

<a href="/cast" class="btn btn-primary btn-sm">kembali</a>

@endsection