<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class castController extends Controller
{
    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('page.tableFilm', ['cast' => $cast]);
    }

    public function create()
    {
        return view('page.cast');
    }//

    public function store(Request $request)
    {
          $request->validate([

              'name' => 'required',
              'umur' => 'required',
              'bio' => 'required'
          
          ]);
          DB::table('cast')->insert([
            'name' => $request['name'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        
        return redirect('/cast');
    }

    public function show($id)
    {
        $cast = DB::table('cast')->find($id);
        
        return view('page.detail' , ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('page.edit', ['cast'=>$cast]);
    }

    public function update($id, Request $request)
    {
        $request->validate([

            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'name' => $request['name'],
                    'umur' => $request ['umur'],
                    'bio' => $request ['bio']
                ]
            );
            return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
